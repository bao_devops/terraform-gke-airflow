provider "helm" {
  kubernetes {
    host     = google_container_cluster.primary.endpoint
    username = var.gke_username
    password = var.gke_password

    token                  = data.google_client_config.current.access_token
    cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)
  }
}

resource "helm_release" "airflow" {
  name       = "airflow"
  repository = "https://airflow.apache.org"
  chart      = "airflow"
  version    = "1.2.0"
  timeout    = 900
  create_namespace = true
  namespace = "airflow"

#  values = [
#    file("${path.module}/kubewatch-values.yaml")
#  ]
}
